package com.devcamp.restapibookauthor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiBookAuthorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiBookAuthorApplication.class, args);
	}

}
